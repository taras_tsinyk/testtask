package com.example.testtask;

import android.app.ActionBar;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;



public class MainActivity extends Activity{
	 public static final String url = "http://echo.jsontest.com/key1/value1/key2/value2/key3/value3/key4/value4";
	    public static ArrayList<String> value;
	    public static final String VAL = "Data";

	    @Override
	    protected void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);
	        setContentView(R.layout.activity_main);

	        new GetResult().execute();

	    }

	    @Override
	    protected void onSaveInstanceState(Bundle outState) {
	        super.onSaveInstanceState(outState);
	        //for safe fetch json's data from URL
	        outState.putStringArrayList(VAL, value);
	    }

	    @Override
	    protected void onRestoreInstanceState(Bundle savedInstanceState) {
	        super.onRestoreInstanceState(savedInstanceState);
	        value = savedInstanceState.getStringArrayList(VAL);
	    }

	    private class GetResult extends AsyncTask<Void, Void, Void> {

	        @Override

	        protected Void doInBackground(Void... params) {

	            if (value == null) {
	                // Creating service handler class instance
	                ServiceHandler sh = new ServiceHandler();
	                // Making a request to url and getting response
	                String jsonStr = sh.makeServiceCall(url, ServiceHandler.GET);
	                value = new ArrayList<String>();
	                if (jsonStr != null) {
	                    try {
	                        JSONObject jsonObj = new JSONObject(jsonStr);
	                        //count from 1, because key number starts from 1
	                        for (int i = 1; i < jsonObj.length() + 1; i++) {
	                            //safe fetch data to Array list and sort
	                            value.add(jsonObj.getString("key" + i));
	                        }
	                    } catch (JSONException e) {
	                        e.printStackTrace();
	                    }

	                } else {
	                    value.add("Can't connect to server");
	                }
	            }
	            return null;

	        }

	        @Override
	        protected void onPostExecute(Void aVoid) {
	            //show data
	            ListView listView = (ListView) findViewById(R.id.listView);

	            ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this,
	                    android.R.layout.simple_list_item_1, value);
	            listView.setAdapter(adapter);
	            super.onPostExecute(aVoid);
	        }
	    }


    
}
